

# Oblig 2

## By Sanjeth Suthahar, Simen Sørensen and Markus Sommervik Edlin

As of now, the biggest structural problem in the code is that most of the code are in one file only (app.py). This makes it hard to debug and finding bugs, and its diffcult to read. More buggy behaviour in the code makes it automatically more vulnerable to security issues, and possible exploits a hacker can make use of. Good refactorization will give multiple benefits, such as the code being more readable, and making it easier to find hidden bugs in the program.

Had problems implementing log out button, but found out eventually.

Refactoring to improve the stucture and have a cleaner code:
* Resources.py - resources like icons and css 
* Message.py - methods relevant to sql and messages (send, get, save etc)

The next part we did was to find a more secure way of storing the passwords. We decided to use password hashing, including a salt. There is of course a lot of different methods of hashing that can be used. This time we used the sha256 hash including a salt that uses sha1 and the 3 first letters in username. When including a salt, it is much harder for a hacker to decrypt (if confidential data somehow has been leaked). See methods passwordHash, hashedPasswords, checkPassword for further details. The login is working and only accepting when username and password match, but unfortunately we get a server internal error and we are not entirely sure why this happens. We still have decided to include this to show a much more secure password handling.  
The most ideal solution wouldve been to store passwords in a database isntead of a list of lists.

## Part 2B

Important notes/security vulnerabilities and improvements:
Due to different reasons, we were far from everything we wanted to implement, and only got to do a few changes to the code. Therefore, it is important to mention what was our plan further with security in focus:

Right now, the sql handling the messages is very vulnerable. The biggest flaw is that it accepts "everything", meaning malicious input can/will be accepted by the program. What this means is that a hacker can take advantage of the input; for example by tricking the program to execute multiple queryes, a query that always is true get access to confidential data in the database, or queries that harms the database and security. This is called sql injection, which is one of the most common web hacking techinques. 
How to fix this: 
Use prepared statements. Then we both have a more efficient code and much more secure. This reduces/eliminates sql injection attacks, since we make sure a dangerous input will be accepted. 
- make sure there isnt multiple unwanted queries happening, check that the input doesnt end with ";".
Other current vulnerabilities in the code is passwords being stored in a list (table) instead of a database, and session cookie only stored as a string which is unsecure. 

Implement secure messaging, fix the big vulnerabilities currently in the code when it comes to messages. 

Design could be improved by a lot. In this exercise, the error code/query is shown to us (the user) for simplicity. In a public website, this would be very bad for security and potential hackers. 
some simple design fixes which improves security: 
Remove error code from sql (this is in general, we are aware that the errors/query is supposed to show for us) 
List of hashed passwords does now get duplicated every time someone logs in, this isnt a good solution but a solution that works for now. Because of a small time constraint, we did not fix this small issue
Storing password in a database
Hide sql messages to other users 



### Questions
* Threat model – who might attack the application? 

The application can be attacked by many different groups/person, it could be an active hacker, people looking to get sensitive information (username/password or others messages), or hackers you would like to harm the application in some way.


* What can an attacker do? / What are the main attack vectors for the application?
there are several attack vectors an attacker might use. Some of the most important are "man in the middle" attacks, malicious injection, phising, brute-force and exploiting insecre design or bugs in the code. 

- Man in the middle attack
One unwanted party suddenly gets information "in the middle", this can be a hacker pretending to be someone else to gather sensitive information, or getting unwanted access where the person is spying on messages sent without any of the parties necessarily knowing.

- SQL injection
See 2A for information about this attack vector. 

- Phising
A method where a hacker tries to trick the user to give data. This is done by for example having a fake link looking similar to our application, but instead gives the data straight to the hacker. 

- Cross-site request forgery
Where the hacker injects malicious and executable scripts to the code.
If cookies is secure implemented, then it should not be possible for someone to send a message on another person`s application. As of right now, this is something that also needs to get fixed in our code.

- Brute-force attack
This is always a potential attack in applications like this. This is of course important to think about when choosing a secure password, so users should be fully aware of this. One way to improve the security could therefore be that the program does not accept " too simple" passwords.

* What can an attacker do? What damage could be done (in terms of confidentiality, integrity, availability)? Are there limits to what an attacker can do? Are there limits to what we can sensibly protect against?  

- Confidentiality:
Damage: attacks like man-in-the-middle and phising to gather sensitive/confidential data. 
Passwords stored secure and encrypted, in our code this can definitely be improved.

- Integrity:
Changing data should not be easy for potential hackers, but insecure design etc could make this happen.

- Availability:
Not very clear, but should be aware of attacks like spoofing or DDoS to potenital shut down the program, make server slower etc.

* Limits to attacker and sensible protections: 
Everything mentioned above are important protections we have to think about and fix by our best capability. There is other security aspects we can do as well, but these are the most important ones.
- We have to think about cost/efficiency when thinking about what security to implement in the application  
- Limits to attacker: as of now, most things doesnt have a good security level, so not many limits in the code. When the program is finished however, sql injection and getting confidential data should be difficult to non-existent problem. Still, we always have to think about the security. Confidential information could get leaked by attack methods already mentioned.

* What should we do (or what have you done) to protect against attacks?
Already written about in 2A and above. To mention short again: secure passwords stored in a secure place, implement password check, eliminate possibilities for sql injection, cookies etc.


* What is the access control model?
The access control model used is a attribute-based access control. We have a login system that authenticates and only gives access if both inputs correlate with each other. Also we have that a user cannot see other peoples messages without them being supposed to.

* How can you know that you security is good enough? (traceability)
With a strong traceability, we can easily see if someone with authentication (or someone who dont) edit or make changes to the application/code. This is important in case some buggy behavious has arrived, and that this gets detected as soon as possible before its too late. 